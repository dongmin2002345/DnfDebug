#include "TwoMachineDebugging.h"

NTSTATUS TwoMachineDebugging::Start_TwoMachineDebugging(PDRIVER_OBJECT pPDriverObj)
{
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	m_Hook.Hook_Init();
	Pass_TP_Security_Component();
	Status = Pass_TP_KdCom_Hook(pPDriverObj);
	Status = Pass_TP_anomaly();
    Pass_TP_KdDebuggerEnabled_Set_0();
	return Status;
}

VOID TwoMachineDebugging::End_TwoMachineDebugging()
{
	SharedUserData->KdDebuggerEnabled = TRUE;
	if (this->m_IsHOOK) {
		m_Hook.UnhookKernelApi((PVOID)m_KdpTrap, m_KdpTrapBytes, m_patch_size);
	}
	
	m_Hook.Un_HookClass();
	KeCancelTimer(&g_PassObjTimer);

}

VOID TwoMachineDebugging::Pass_TP_Security_Component()
{
	SharedUserData->KdDebuggerEnabled = FALSE;
}

NTSTATUS TwoMachineDebugging::Pass_TP_KdCom_Hook(PDRIVER_OBJECT pPDriverObj)
{
	return FastFunction::HideDriver("kdcom.dll", pPDriverObj);
}

NTSTATUS TwoMachineDebugging::Pass_TP_anomaly()
{
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	m_KdpTrap = Get_KdpTrapAddr();
	if (m_KdpTrap == 0) {
		return Status;
	}
	Status = HookKdpTrap(this->m_KdpTrap);
	return Status;
	
}

ULONGLONG TwoMachineDebugging::Get_KdpTrapAddr()
{

	ULONGLONG ntoskrnBaseAddr = FastFunction::GetSystemModuleBase("ntoskrnl.exe");
	/*
	   1803 8192CC
	   1803 ���������� = 0x8082CC
	*/
	ULONGLONG OffSet = 0x8192CC;
	return ntoskrnBaseAddr + OffSet;
}
NTSTATUS TwoMachineDebugging::HookedKdpTrap(
	IN PKTRAP_FRAME TrapFrame,
	IN PKEXCEPTION_FRAME ExceptionFrame,
	IN PEXCEPTION_RECORD ExceptionRecord,
	IN PCONTEXT ContextRecord,
	IN KPROCESSOR_MODE PreviousMode,
	IN BOOLEAN SecondChanceException
) 
{

	PEPROCESS hp = PsGetCurrentProcess();
	if (!_stricmp((char *)PsGetProcessImageFileName(hp), "TASLogin.exe"))
	{
		return STATUS_SUCCESS;
	}

	return OriginalKdpTrap(TrapFrame, ExceptionFrame, ExceptionRecord, ContextRecord, PreviousMode, SecondChanceException);
}
NTSTATUS TwoMachineDebugging::HookKdpTrap(ULONGLONG KdpTrap)
{
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	if (KdpTrap == 0) {
		return Status;
	}

	m_KdpTrapBytes = m_Hook.HookKernelApi((PVOID)KdpTrap, (PVOID)HookedKdpTrap, (PVOID*)&OriginalKdpTrap, &m_patch_size);
	if (m_KdpTrapBytes == NULL) {
		return Status;
	}
	m_IsHOOK = TRUE;
	Status = STATUS_SUCCESS;
	return Status;
}
VOID
PassObj(
	__in struct _KDPC  *Dpc,
	__in_opt PVOID  DeferredContext,
	__in_opt PVOID  SystemArgument1,
	__in_opt PVOID  SystemArgument2
)
{
	__try
	{
	
		*KdDebuggerEnabled = 0x1;
	}
	__except (1)
	{
		return;
	}
	KeSetTimer(&g_PassObjTimer, g_PassObjTime, &g_PassObjDpc);
	return;
}
VOID TwoMachineDebugging::Pass_TP_KdDebuggerEnabled_Set_0()
{
	
	g_PassObjTime.QuadPart = -10000;
	KeInitializeTimer(&g_PassObjTimer);
	KeInitializeDpc(&g_PassObjDpc, &PassObj, NULL);
	KeSetTimer(&g_PassObjTimer, g_PassObjTime, &g_PassObjDpc);
	
}


