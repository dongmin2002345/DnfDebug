#include "DNFDebug.h"

NTSTATUS DNFDebug::DNFDebugStart(PDRIVER_OBJECT pPDriverObj)
{
	NTSTATUS Status = this->m_R3_ReadProcess.R3_ReadProcess_Start(pPDriverObj);
	Status = m_TwoMachineDebugging.Start_TwoMachineDebugging(pPDriverObj);
	Status = m_GameDebug.Start_GanmeDebug();
	return Status;
}

VOID DNFDebug::UnDebugStart()
{
	m_TwoMachineDebugging.End_TwoMachineDebugging();
	this->m_R3_ReadProcess.UnLoad_R3_ReadProcess();
	m_GameDebug.Un_GanmeDebug();
}
