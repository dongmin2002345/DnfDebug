#pragma once
#include "R3_ReadProcess.h"
#include "TwoMachineDebugging.h"
#include "GameDebug.h"
class DNFDebug
{
public:
	NTSTATUS DNFDebugStart(PDRIVER_OBJECT pPDriverObj);
	VOID UnDebugStart();

private:
	R3_ReadProcess m_R3_ReadProcess;
	TwoMachineDebugging m_TwoMachineDebugging;
	GameDebug m_GameDebug;
};

